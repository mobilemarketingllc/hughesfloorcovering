
<div class="product-variations">
    <h3>Color Variations</h3>
    <?php
    global $post;
    $flooringtype = $post->post_type;
    $collection = get_field('collection');
    $familysku = get_post_meta($post->ID, 'collection', true);
    $familycolor = get_post_meta($post->ID, 'style', true);  

    if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall') { 
    $args = array(
        'post_type'      => $flooringtype,
        'posts_per_page' => -1,
        'post_status'    => 'publish',
        'meta_query'     => array(
            array(
                'key'     => 'style',
                'value'   => $familycolor,
                'compare' => '='
            ),
            array(
                'key' => 'swatch_image_link',
                'value' => '',
                'compare' => '!='
                )
        )
    ); 
    }else{

        $args = array(
            'post_type'      => $flooringtype,
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => 'collection',
                    'value'   => $familysku,
                    'compare' => '='
                ),
                array(
                    'key' => 'swatch_image_link',
                    'value' => '',
                    'compare' => '!='
                    )
            )
        );

    }
    ?>
    <?php
    $the_query = new WP_Query($args);
    ?>
    <div class="color_variations_slider">
        <div class="slides">
            <?php
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
            ?>
            <div class="slide col-md-2 col-sm-3 col-xs-6 color-box">
                <figure class="color-boxs-inner">
					<div class="color-boxs-inners">
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo $image; ?>" style="<?php echo $style; ?>" class="swatch-img tooltipped" data-position="top" data-delay="50" data-tooltip="<?php the_title(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" width="100" height="100" />
                        </a>
                        <br />
                        <small><?php the_field('color'); ?></small>
                    </div>
                </figure>
            </div>
            <?php
    } ?>
        </div>
    </div>
</div>
<?php wp_reset_postdata(); ?> 